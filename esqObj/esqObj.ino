#include <Servo.h>;

Servo ruedaIzq, ruedaDch;

Servo servoUs;

int irIzq = 3;
int irDch = 2;

int triPin = 4;
int echoPin = 5;

int buzzer = 12;

void setup() {
  Serial.begin(9600);
  
  pinMode(irIzq, INPUT);
  pinMode(irDch, INPUT);
  
  pinMode(triPin, OUTPUT);
  pinMode(echoPin, INPUT);

  pinMode(buzzer, OUTPUT);

  ruedaIzq.attach(9);
  ruedaDch.attach(8);

  servoUs.attach(11);
  
}

void loop() {
  
  // UltraSonidos - Dch
  servoUs.write(140);
  delay(150);
  digitalWrite(triPin, LOW);
  delayMicroseconds(5);
  digitalWrite(triPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(triPin, LOW);
  
  long duracionDch, distanciaDch;
  duracionDch = pulseIn(echoPin, HIGH);
  
  distanciaDch = duracionDch / 2 / 29.1;
  Serial.println(String(distanciaDch) + " Dch cm.");
  
  // UltraSonidos - Izq
  servoUs.write(40);
  delay(200);
  digitalWrite(triPin, LOW);
  delayMicroseconds(5);
  digitalWrite(triPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(triPin, LOW);
  
  long duracionIzq, distanciaIzq;
  duracionIzq = pulseIn(echoPin, HIGH);
  
  distanciaIzq = duracionIzq / 2 / 29.1;
  Serial.println(String(distanciaIzq) + " IZQ cm.");
 
  // Decision
  long diferencia = distanciaDch - distanciaIzq;
  if (distanciaDch < 25 || distanciaIzq < 25){
    decision(diferencia);
  } else {
    alante();
  }
  
  // UltraSonidos - Md
  servoUs.write(90);
  delay(150);
  digitalWrite(triPin, LOW);
  delayMicroseconds(5);
  digitalWrite(triPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(triPin, LOW);
  
  long duracionMd, distanciaMd;
  duracionMd = pulseIn(echoPin, HIGH);
  
  distanciaMd = duracionMd / 2 / 29.1;
  Serial.println(String(distanciaMd) + " Md cm.");
  
  if (!digitalRead(irIzq) || !digitalRead(irDch) || distanciaDch < 7 || distanciaDch < 7 || distanciaDch < 7 || ) {
    atras();
    delay(500);
    girarDch();
    delay(1000);
    
  } else {
    alante();
    
  }
  
}


void decision(long diferencia) {
  if (diferencia < 0) { // mas  va - menos no va
    girarIzq();
  } else {
    girarDch();
  }
}

void girarDch() {
    ruedaIzq.write(180);
    ruedaDch.write(90);
}

void girarIzq() {
    ruedaIzq.write(90);
    ruedaDch.write(0);
}

void atras() {
    ruedaIzq.write(0);
    ruedaDch.write(180);
}

void alante() {
    ruedaIzq.write(180);
    ruedaDch.write(0);
}

