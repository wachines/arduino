#include <Servo.h>;

Servo ruedaIzq, ruedaDch;
Servo servoUs;

int irIzq = 3;
int irDch = 2;

int triPin = 4;
int echoPin = 5;

void setup() {
  Serial.begin(9600);
  
  pinMode(irIzq, INPUT);
  pinMode(irDch, INPUT);

  ruedaIzq.attach(9);
  ruedaDch.attach(8);
  
  pinMode(triPin, OUTPUT);
  pinMode(echoPin, INPUT);
  servoUs.attach(11);
}



void loop() {
  // UltraSonidos - Md
  servoUs.write(90);
  digitalWrite(triPin, LOW);
  delayMicroseconds(5);
  digitalWrite(triPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(triPin, LOW);
  
  long duracionMd, distanciaMd;
  duracionMd = pulseIn(echoPin, HIGH);
  
  distanciaMd = duracionMd / 2 / 29.1;
  
  if (!digitalRead(irIzq) || !digitalRead(irDch) || distanciaMd < 7) {
    atras();
    delay(800);
    girarDch();
    delay(1000);
    
  } else {
    alante();
    
  }
}

void esquive(long distancia) {
    if (distancia < 25 && distancia >= 1) {
      girarDch();
      delay(700);
    } else {
      girarDch();
      delay(100);
    }
}

void girarDch() {
    ruedaIzq.write(180);
    ruedaDch.write(90);
}

void girarIzq() {
    ruedaIzq.write(90);
    ruedaDch.write(0);
}

void atras() {
    ruedaIzq.write(0);
    ruedaDch.write(180);
}

void alante() {
    ruedaIzq.write(180);
    ruedaDch.write(0);
}

