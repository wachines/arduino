#include <Servo.h>;

Servo ruedaIzq;
Servo ruedaDch;

Servo servoUs;

int irIzq = 3;
int irDch = 2;

int triPin = 4;
int echoPin = 5;

int buzzer = 12;


void setup() {
  Serial.begin(9600);
  
  pinMode(irIzq, INPUT);
  pinMode(irDch, INPUT);
  
  pinMode(triPin, OUTPUT);
  pinMode(echoPin, INPUT);

  pinMode(buzzer, OUTPUT);

  ruedaIzq.attach(9);
  ruedaDch.attach(8);

  servoUs.attach(11);
  
}

void loop() {
  
  // UltraSonidos - Md
  servoUs.write(90);
  digitalWrite(triPin, LOW);
  delayMicroseconds(5);
  digitalWrite(triPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(triPin, LOW);
  
  long duracionDch, distanciaDch;
  duracionDch = pulseIn(echoPin, HIGH);
  
  distanciaDch = duracionDch / 2 / 29.1;
  Serial.println(String(distanciaDch) + " Dch cm.");
  
  // condiciones
  if (distanciaDch < 15 && distanciaDch >= 1) {
    ruedaIzq.write(90);
    ruedaDch.write(90);
    
  } else if (digitalRead(irIzq)) {
    girarDch();
    
  } else if (digitalRead(irDch)) {  
    girarIzq();
    
  } else if (!digitalRead(irIzq) && !digitalRead(irDch)) {
    alante();
  }
}

void girarDch() {
    ruedaIzq.write(180);
    ruedaDch.write(90);
}

void girarIzq() {
    ruedaIzq.write(90);
    ruedaDch.write(0);
}

void alante() {
    ruedaIzq.write(180);
    ruedaDch.write(0);
}

